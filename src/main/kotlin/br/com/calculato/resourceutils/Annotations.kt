package br.com.calculato.resourceutils

/**
 * API marked with this annotation is for in service calls or API calls from same domain services.
 * This may not be available at public API access. This service does not check tenant permissions
 *
 * Please take extremely care if using this API.
 */
@RequiresOptIn(
    message = "This may not be available at public API access. This service does not check tenant permissions",
    level = RequiresOptIn.Level.WARNING)
@Retention(AnnotationRetention.BINARY)
@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
annotation class InternalService

/**
 * Annotation to define metadata for this client data model
 */
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class ClientModel(
    /**
     * Version of the server data model that matches this model
     */
    val serverVersion: String
)

/**
 * Annotation to define metadata for data classes that serve content to microservices clients at
 * the same domain
 */
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
annotation class ServerModel(
    /**
     * Current [version] of this model class, every breaking change should have it`s own version
     */
    val version: String,
    /**
     * A list of microservices names in the domain that make use of this model, signaling where to update
     * if this model changes
     */
    val msDependents: Array<String>
)