package br.com.calculato.resourceutils

import java.util.Base64

/**
 * Helper function to encode [this] string to a base 64 encoded string
 */
fun String.encodeToBase64() = Base64.getEncoder().encodeToString(this.toByteArray())

/**
 * Helper function to decode [this] base 64 encoded string
 */
fun String.decodeFromBase64() = Base64.getDecoder().decode(this).toString(Charsets.UTF_8)
